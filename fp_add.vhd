library IEEE;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity FP_ADDER is
port(   fp_a	  : in std_logic_vector(31 downto 0);
	fp_b 	  : in std_logic_vector(31 downto 0);
	en 	  : in std_logic;
	clk 	  : in std_logic;
	operator  : in std_logic; -- IGNORE!
	overflow  : out std_logic;
	zero 	  : out std_logic;
	fp_result : out std_logic_vector(31 downto 0));
end entity FP_ADDER;

architecture behav of FP_ADDER is
	signal debug1 : std_logic;
	signal debug2 : std_logic;
	signal debug3 : std_logic;
	signal debug_rslt : std_logic_vector(31 downto 0);
	signal debug_sign : std_logic;
	signal debug_exp  : std_logic_vector(7 downto 0);
	signal debug_fract : std_logic_vector(24 downto 0);
begin
process(clk) 
	variable sign_a   	 : std_logic;
	variable sign_b   	 : std_logic;
	variable sign_tmp 	 : std_logic;
	
	variable exp_a    	 : unsigned(7  downto 0);
	variable exp_b         	 : unsigned(7  downto 0);
	variable exp_result 	 : unsigned(7  downto 0);
	variable exp_tmp  	 : unsigned(7  downto 0);
	variable align_counter 	 : unsigned(7  downto 0);
	
	variable fract_a  	 : unsigned(24 downto 0);
	variable fract_b  	 : unsigned(24 downto 0);
	variable fract_tmp 	 : unsigned(24 downto 0);
	variable fract_result 	 : unsigned(24 downto 0);

	variable tmp_result    	 : std_logic_vector(31 downto 0);
	variable overflow_result : std_logic;
begin
	if clk'event and clk='1' then
		if en = '1' then
			-- extract the sign, exp and fract fields from fp_a
			-- the implicity 1 of fraction field must be included
			-- additional bit 24 helps to detect fraction add overflow
			sign_a  := fp_a(31);
			exp_a   := unsigned(fp_a(30 downto 23));
			fract_a := unsigned('0' & '1' & fp_a(22 downto 0));

			-- do same for fp_b
			sign_b  := fp_b(31);
			exp_b   := unsigned(fp_b(30 downto 23));
			fract_b := unsigned('0' & '1' & fp_b(22 downto 0));

			-- Initialize overflow to zero
			overflow_result := '0';

			-- Align fractions and add them up
			if (exp_a /= exp_b) then
				if(exp_a > exp_b) then	-- swap a & b
					exp_tmp := exp_a;
					exp_a   := exp_b;
					exp_b   := exp_tmp;
			
					fract_tmp := fract_a;
					fract_a   := fract_b;
					fract_b   := fract_tmp;
				end if;
			
				align_counter := exp_b - exp_a;
			
				while (align_counter /= 0) loop
					fract_a := unsigned('0' & fract_a(24 downto 1));
					align_counter := align_counter - 1;
				end loop;
			end if;
			
			exp_result := exp_b;
			fract_result := fract_a + fract_b;

			debug1 <= '1';
			-- Normalize results (fraction overflow correction)
			if (fract_result(24) = '1') then
				-- overflow may happen here
				if (exp_result = 254) then
					overflow_result := '1';
				end if;

				fract_result := unsigned('0' & fract_result(24 downto 1));
				exp_result   := exp_result + 1;
			end if;

			fp_result(31) 		<= sign_a;
			fp_result(30 downto 23) <= std_logic_vector(exp_result);
			fp_result(22 downto  0) <= std_logic_vector(fract_result(22 downto 0));
		
			debug_sign <= sign_a;
			debug_fract <= std_logic_vector(fract_result);
			debug_exp <= std_logic_vector(exp_result);
			overflow <= overflow_result;
			zero 	  <= '0';

		end if; -- end if en = '1'
	end if; 	-- end if clk'event
end process;
end behav;
