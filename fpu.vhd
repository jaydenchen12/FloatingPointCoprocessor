library IEEE;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

-- TODO: thoroughly test add & subtract
-- TODO: write fp_mult
-- TODO: write fp_div

entity FPU is
port(   fp_a	  : in std_logic_vector(31 downto 0);
	fp_b	  : in std_logic_vector(31 downto 0);
	clk	  : in std_logic;
	funct	  : in std_logic_vector(5 downto 0);
	zero	  : out std_logic;
	overflow  : out std_logic;
	underflow : out std_logic;
	fp_result : out std_logic_vector(31 downto 0));
end entity FPU;

-- funct	operation

-- 00 0001	ADD
-- 00 0010	SUBTRACT
-- 00 0011	MULTIPLICATION
-- 00 0100	DIVISION

architecture behav of FPU is

	component FP_ADDER is
	port(   fp_a	  : in std_logic_vector(31 downto 0);
		fp_b 	  : in std_logic_vector(31 downto 0);
		en 	  : in std_logic;
		clk 	  : in std_logic;
		operator  : in std_logic; -- IGNORE!
		overflow  : out std_logic;
		zero 	  : out std_logic;
		fp_result : out std_logic_vector(31 downto 0));
	end component;

	component FP_SUBTRACTOR is
	port(   fp_a	   : in std_logic_vector(31 downto 0);
		fp_b 	   : in std_logic_vector(31 downto 0);
		en 	   : in std_logic;
		clk 	   : in std_logic;
		operator   : in std_logic; -- IGNORE!
		underflow  : out std_logic;
		zero 	   : out std_logic;
		fp_result  : out std_logic_vector(31 downto 0));
	end component;

	component FP_MULTIPLIER is
	port( 	fp_a 	  : in std_logic_vector(31 downto 0);
		fp_b 	  : in std_logic_vector(31 downto 0);
		clk 	  : in std_logic;
		en 	  : in std_logic;
		zero 	  : out std_logic;
		overflow  : out std_logic;
		underflow : out std_logic;
		fp_result : out std_logic_vector(31 downto 0));
	end component;

	component FP_DIVIDER is
	port( 	fp_a 	  : in std_logic_vector(31 downto 0);
		fp_b 	  : in std_logic_vector(31 downto 0);
		clk 	  : in std_logic;
		en 	  : in std_logic;
		zero 	  : out std_logic;
		overflow  : out std_logic;
		underflow : out std_logic;
		fp_result : out std_logic_vector(31 downto 0));
	end component;

	signal in1, in2 : std_logic_vector(31 downto 0);
	signal result : std_logic_vector(31 downto 0);
	signal add_en, sub_en, mult_en, div_en : std_logic;

	-- output signals
	signal add_rslt, sub_rslt, mult_rslt, div_rslt : std_logic_vector(31 downto 0);
	signal sub_uf, mult_uf, div_uf : std_logic;
	signal add_of, mult_of, div_of : std_logic;

begin
	-- wire everything here
	add  : FP_ADDER      port map(in1, in2, add_en, clk, 	'U',   add_of, 	zero, 	 add_rslt);
	sub  : FP_SUBTRACTOR port map(in1, in2, sub_en, clk, 	'U',   sub_uf, 	zero, 	 sub_rslt);
	mult : FP_MULTIPLIER port map(in1, in2, clk, 	mult_en, zero, mult_of, mult_uf, mult_rslt);
	div  : FP_DIVIDER    port map(in1, in2, clk, 	div_en,  zero, div_of, 	div_uf,  div_rslt);

	zero <= '0';

process(clk, funct) begin
	if clk'event and clk='1' then
		if (funct = "000001") then
			if(fp_a(31) = fp_b(31)) then -- add
				add_en  <= '1';
				sub_en  <= '0';
				mult_en <= '0';
				div_en  <= '0';
				in1     <= fp_a;
				in2     <= fp_b;
				overflow  <= add_of;
				underflow <= 'U';
				fp_result <= add_rslt;
				-- add: a + b
			elsif(fp_a(31) = '1') then -- (-a) + (+b) --> (b - a)
				add_en   <= '0';
				sub_en   <= '1';
				mult_en  <= '0';
				div_en   <= '0';
				in1 	 <= fp_b;
				in2 	 <= fp_a;
				in2(31)	 <= '0';
				underflow <= sub_uf;
				overflow  <= 'U';
				fp_result <= sub_rslt;
				-- fp_a(31) <= 0
				-- sub: b - a
			else			   -- (a) + (-b) --> a - b
				add_en   <= '0';
				sub_en   <= '1';
				mult_en  <= '0';
				div_en   <= '0';
				in1 	 <= fp_a;
				in2 	 <= fp_b;
				in2(31)	 <= '0';
				underflow <= sub_uf;
				overflow  <= 'U';
				fp_result <= sub_rslt;
				-- fp_b(31) <= 0
				-- sub: a - b
			end if;
		elsif (funct = "000010") then -- subtract
			if(fp_a(31) = fp_b(31)) then
				add_en  <= '0';
				sub_en  <= '1';
				mult_en <= '0';
				div_en  <= '0';
				in1 	<= fp_a;
				in2 	<= fp_b;
				underflow <= sub_uf;
				overflow  <= 'U';
				fp_result <= sub_rslt;
			elsif(fp_a(31) = '1') then  -- (-a) - (+b) --> (-a) + (-b) 
				add_en   <= '1';
				sub_en   <= '0';
				mult_en  <= '0';
				div_en   <= '0';
				in1 	 <= fp_a;
				in2 	 <= fp_b;
				in2(31)	 <= '0';
				overflow  <= add_of;
				underflow <= 'U';
				fp_result <= add_rslt;
				-- fp_a(31) <= 0
				-- add: b + a
			else			    -- (+a) - (-b) --> (+a) + (+b)
				add_en   <= '1';
				sub_en   <= '0';
				mult_en  <= '0';
				div_en   <= '0';
				in1 	 <= fp_a;
				in2 	 <= fp_b;
				in2(31)  <= '0';
				overflow  <= add_of;
				underflow <= 'U';
				fp_result <= add_rslt;
				-- fp_b(31) <= 0
				-- add: a + b
			end if;

		elsif (funct = "000011") then -- mult
			add_en    <= '0';
			sub_en    <= '0';
			mult_en   <= '1';
			div_en    <= '0';
			in1 	  <= fp_a;
			in2 	  <= fp_b;
			overflow  <= mult_of;
			underflow <= mult_uf;
			fp_result <= mult_rslt;

		elsif (funct = "000011") then -- div
			add_en    <= '0';
			sub_en    <= '0';
			mult_en   <= '0';
			div_en    <= '1';
			in1 	  <= fp_a;
			in2 	  <= fp_b;
			overflow  <= div_of;
			underflow <= div_uf;
			fp_result <= div_rslt;
		else
			add_en    <= '0';
			sub_en    <= '0';
			mult_en   <= '0';
			div_en    <= '0';
			in1 	  <= 'U';
			in2 	  <= 'U';
			overflow  <= 'U';
			underflow <= 'U';
			fp_result <= 'U';

		end if;
	end if;
end process;
end behav;
