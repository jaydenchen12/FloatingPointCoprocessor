library IEEE;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity FP_MULTIPLIER is
port( 	fp_a 	  : in std_logic_vector(31 downto 0);
	fp_b 	  : in std_logic_vector(31 downto 0);
	clk 	  : in std_logic;
	en 	  : in std_logic;
	zero 	  : out std_logic;
	overflow  : out std_logic;
	underflow : out std_logic;
	fp_result : out std_logic_vector(31 downto 0));
end entity FP_MULTIPLIER;

architecture behav of FP_MULTIPLIER is begin
process(clk) 
begin
	-- i=0
	-- Product(63 downto 32) = (others=>'0')
	-- Product(31 downto 0) = Multiplier

	-- while (i < 32)
	-- {
	--	if(Product(0) = '1')
	--		Product(63 downto 32), CarryOut = 
	--			Product(63 downto 32) + Multiplicand
	--	product = CarryOut & Product(63 downto 1);
	--	i++;
	-- }
end process;
end behav;
