library IEEE;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity FP_DIVIDER is
port( 	fp_a 	  : in std_logic_vector(31 downto 0); -- dividend
	fp_b 	  : in std_logic_vector(31 downto 0); -- divisor
	clk 	  : in std_logic;
	en 	  : in std_logic;
	zero 	  : out std_logic;
	overflow  : out std_logic;
	underflow : out std_logic;
	fp_result : out std_logic_vector(31 downto 0));
end entity FP_DIVIDER;

architecture behav of FP_DIVIDER is begin
process(clk)

	variable remainder : unsigned(63 downto 0) := (others => '0');
	variable divisor   : unsigned(31 downto 0);
	variable dividend  : unsigned(31 downto 0);

	variable sign_a    : std_logic;
	variable sign_b    : std_logic;

	variable exp_a      : unsigned(7 downto 0);
	variable exp_b      : unsigned(7 downto 0);
	variable exp_result : unsigned(7 downto 0);

	variable fract_a   : unsigned(24 downto 0);
	variable fract_b   : unsigned(24 downto 0);

begin
	if clk'event and clk='1' then
		if en = '1' then

			-- ************************************************ --
			-- **** FOLLOW DIVISON FLOW CHART FROM HANDOUT **** --
			-- ************************************************ --
			
			-- get signs, exponents, and fractions
			sign_a  := fp_a(31);
			exp_a   := unsigned(fp_a(30 downto 23));
			fract_a := unsigned('0' & '1' & fp_a(22 downto 0));

			-- do same for fp_b
			sign_b  := fp_b(31);
			exp_b   := unsigned(fp_b(30 downto 23));
			fract_b := unsigned('0' & '1' & fp_b(22 downto 0));

			-- subtract exponents, add bias
			exp_result = exp_a - exp_b + 127;
			

			-- use the below code (provided by Shihao) to divide the significands
			-- note- this is only psuedo code! things will need to be changed

			-- remainder(31 downto 0) := dividend;
			-- remainder := remainder(62 downto 0) & '0';
				
			-- for i in 0 to 32 loop
			--	if(divisor <= (remainder(63 downto 32) - divisor)) then
			-- 		remainder(63 downto 32) := remainder(63 downto 32) - divisor;
			--		remainder := remainder(62 downto 0) & '1';
			--	else
			--		remainder := remainder(62 downto 0) & '0';
			--	end if;
			--end loop;

			-- TODO: handle over/underflow
			-- TODO: normalize
			-- TODO: round

			--fp_result <= std_logic_vector(remainder(31 downto 0));
		end if;
	end if;
end process;
end behav;

